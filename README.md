Role Name
=========

A role that installs Grafana, <https://grafana.com/grafana/>

Role Variables
--------------

The most important variables are listed below:

``` yaml
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
